import { Entity, PrimaryKey, Property } from '@mikro-orm/core';
import { randomBytes } from 'crypto';
import { safeJsonParse } from '../../common';

interface RawToken {
  d: string;
  e: number;
}

@Entity()
export class OneTimeToken {
  @PrimaryKey()
  public id: number;

  @Property({ index: true })
  public value: string;

  @Property({ type: Date, defaultRaw: 'NOW()' })
  public createdAt!: Date;

  @Property({ type: Date, defaultRaw: 'NOW()', onUpdate: () => new Date() })
  public updatedAt!: Date;

  constructor(value: string) {
    this.value = value;
  }

  /**
   *
   * @param ttl - time in ms
   */
  public static generate(ttl: number): OneTimeToken {
    // We don't need to sign the token because we will always look it up in the database as it is for one time use
    const token = {
      d: randomBytes(64).toString('hex'),
      e: Date.now() + ttl,
    } as RawToken;

    const value = Buffer.from(JSON.stringify(token)).toString('base64');

    return new OneTimeToken(value);
  }

  public isValid(): boolean {
    const json = Buffer.from(this.value, 'base64').toString('utf8');

    const data = safeJsonParse<RawToken>(json);

    return data?.e > Date.now();
  }
}
