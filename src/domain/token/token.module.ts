import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { OneTimeToken } from './one-time-token.entity';
import { TokenService } from './token.service';

@Module({
  imports: [MikroOrmModule.forFeature([OneTimeToken])],
  providers: [TokenService],
  exports: [TokenService],
})
export class DomainTokenModule {}
