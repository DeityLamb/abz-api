import { InjectRepository } from '@mikro-orm/nestjs';
import { EntityManager, SqlEntityRepository } from '@mikro-orm/postgresql';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { OneTimeToken } from './one-time-token.entity';

@Injectable()
export class TokenService {
  private readonly DEFAULT_TTL = 1000 * 60 * 40;

  constructor(
    @InjectRepository(OneTimeToken)
    private readonly tokens: SqlEntityRepository<OneTimeToken>,
    private readonly em: EntityManager,
  ) {}

  public async txUse(em: EntityManager, value: string): Promise<void> {
    const tokens = em.repo(OneTimeToken);

    const token = await tokens.findOne({ value });

    if (!token?.isValid()) {
      throw new UnauthorizedException('The token expired.');
    }

    em.remove(token);
  }

  public async generate(): Promise<OneTimeToken> {
    const token = OneTimeToken.generate(this.DEFAULT_TTL);

    await this.em.persistAndFlush(token);

    return token;
  }
}
