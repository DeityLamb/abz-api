import { MikroOrmModule } from '@mikro-orm/nestjs';
import { PostgreSqlDriver } from '@mikro-orm/postgresql';
import { Module } from '@nestjs/common';

@Module({
  imports: [
    MikroOrmModule.forRoot({
      entities: ['./dist/domain/**/*.entity.js'],
      entitiesTs: ['./src/domain/**/*.entity.ts'],
      driver: PostgreSqlDriver,
      debug: process.env.MIKRO_ORM_DEBUG === 'true',
      clientUrl: process.env.POSTGRES_CONNECTION as string,
    }),
  ],
})
export class DomainModule {}
