import { MikroOrmModule } from '@mikro-orm/nestjs';
import { Module } from '@nestjs/common';
import { DomainTokenModule } from '../token';
import { Position } from './position.entity';
import { User } from './user.entity';
import { UserService } from './user.service';

const OrmFeatures = MikroOrmModule.forFeature([User, Position]);

@Module({
  imports: [OrmFeatures, DomainTokenModule],
  providers: [UserService],
  exports: [OrmFeatures, UserService],
})
export class UserDomainModule {}
