import { Entity, ManyToOne, PrimaryKey, Property } from '@mikro-orm/core';
import { Position } from './position.entity';

@Entity()
export class User {
  @PrimaryKey()
  public id: number;

  @Property()
  public name: string;

  @Property()
  public email: string;

  @Property()
  public phone: string;

  @Property()
  public photo: string;

  @ManyToOne({ entity: () => Position, eager: true })
  public position: Position;

  @Property({ type: Date, defaultRaw: 'NOW()' })
  public createdAt!: Date;

  @Property({ type: Date, defaultRaw: 'NOW()', onUpdate: () => new Date() })
  public updatedAt!: Date;
}
