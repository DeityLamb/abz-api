import {
  Collection,
  Entity,
  OneToMany,
  PrimaryKey,
  Property,
} from '@mikro-orm/core';
import { User } from './user.entity';

@Entity()
export class Position {
  @PrimaryKey()
  public id: number;

  @Property()
  public name: string;

  @OneToMany({ entity: () => User, lazy: true, mappedBy: 'position' })
  public users: Collection<User>;

  @Property({ type: Date, defaultRaw: 'NOW()' })
  public createdAt!: Date;

  @Property({ type: Date, defaultRaw: 'NOW()', onUpdate: () => new Date() })
  public updatedAt!: Date;
}
