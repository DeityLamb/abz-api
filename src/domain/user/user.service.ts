import { InjectRepository } from '@mikro-orm/nestjs';
import {
  EntityManager,
  FindOptions,
  SqlEntityRepository,
} from '@mikro-orm/postgresql';
import {
  BadRequestException,
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { randomUUID } from 'crypto';
import sizeOf from 'image-size';
import { join } from 'path';
import tinify from 'tinify';
import { TokenService } from '../token';
import { Position } from './position.entity';
import { User } from './user.entity';

interface CreateUserOptions {
  name: string;
  email: string;
  phone: string;
  position_id: number;
  photo: Express.Multer.File;
  token: string;
}

@Injectable()
export class UserService {
  public static PHOTO_SIZE = 70;

  constructor(
    @InjectRepository(Position)
    private readonly positions: SqlEntityRepository<Position>,
    @InjectRepository(User)
    private readonly users: SqlEntityRepository<User>,
    private readonly tokenService: TokenService,
    private readonly em: EntityManager,
  ) {}

  public async findOne(id: number): Promise<User> {
    const user = await this.users.findOne(id);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user;
  }

  public async pages(query: FindOptions<User>): Promise<[User[], number]> {
    const [users, total_users] = await this.users.findAndCount({}, query);

    if (!users?.length) {
      throw new NotFoundException('Page not found.');
    }

    return [users, total_users];
  }

  public async create(options: CreateUserOptions): Promise<User> {
    const em = this.em.fork();

    try {
      await em.begin();

      const users = em.repo(User);

      const exists = await users.findOne(
        {
          $or: [
            { email: options.email },
            { phone: options.phone },
            { name: options.name },
          ],
        },
        { fields: ['id'] },
      );

      if (exists) {
        throw new ConflictException(
          'User with this phone or email already exist.',
        );
      }

      const position = await this.positions.findOne(options.position_id);

      if (!position) {
        throw new NotFoundException('Position not found.');
      }

      await this.tokenService.txUse(em, options.token);

      const url = await this.uploadPhoto(options.photo.buffer);

      const user = users.create({
        name: options.name,
        email: options.email,
        phone: options.phone,
        position,
        photo: url.toString(),
      });

      em.persistAndFlush(user);

      await em.commit();
      return user;
    } catch (e) {
      em.rollback();
      throw e;
    }
  }

  // We should extract this functionality. But I hope this will be enough for the demo
  private async uploadPhoto(buffer: Buffer): Promise<URL> {
    const filename = randomUUID() + '.jpg';

    const size = sizeOf(buffer);

    if (size.width < 70 || size.height < 70) {
      throw new BadRequestException('Image is invalid.');
    }

    await tinify
      .fromBuffer(buffer)
      .resize({
        method: 'thumb',
        width: UserService.PHOTO_SIZE,
        height: UserService.PHOTO_SIZE,
      })
      .convert({ type: 'image/jpg' })
      .toFile(join('uploads', 'photo', filename));

    return new URL(`/uploads/photo/${filename}`, process.env.SERVICE_URL);
  }

  public getPositions(): Promise<Position[]> {
    return this.positions.findAll({ orderBy: { name: 'ASC' } });
  }
}
