import { IsInt, Max, Min, ValidateIf } from 'class-validator';

export class UsersQuery {
  @IsInt()
  @Min(1)
  @ValidateIf((o) => !o.offset)
  public page: number;

  @IsInt()
  @Min(1)
  public offset: number;

  @IsInt()
  @Min(1)
  @Max(100)
  public count = 5;
}
