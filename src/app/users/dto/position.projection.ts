import { Position } from '../../../domain/user';

export class PositionProjection {
  id: number;
  name: string;

  public static fromEntity(position: Position): PositionProjection {
    return {
      id: position.id,
      name: position.name,
    };
  }
}
