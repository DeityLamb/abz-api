import { IsEmail, IsInt, Length, Matches, Min } from 'class-validator';

export class CreateUserDto {
  @Length(2, 60)
  name: string;

  // OpenAPI specified email format as RFC2822
  // I hope IsEmail decorator will be enough for demo
  @IsEmail()
  email: string;

  @Matches(/^[\+]{0,1}380([0-9]{9})$/)
  phone: string;

  @IsInt()
  @Min(1)
  position_id: number;
}
