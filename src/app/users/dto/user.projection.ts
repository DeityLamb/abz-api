import { User } from '../../../domain/user';

export class UserProjection {
  id: number;
  name: string;
  email: string;
  phone: string;
  position: string;
  position_id: number;
  registration_timestamp: number;
  photo: string;

  public static fromEntity(user: User): UserProjection {
    return {
      id: user.id,
      name: user.name,
      email: user.email,
      phone: user.phone,
      position: user.position.name,
      position_id: user.position.id,
      registration_timestamp: Math.ceil(user.createdAt.getTime() / 1000),
      photo: user.photo,
    };
  }
}
