import { Module } from '@nestjs/common';
import { UserDomainModule } from '../../domain/user';
import { UsersController } from './users.controller';

@Module({
  imports: [UserDomainModule],
  controllers: [UsersController],
})
export class UsersModule {}
