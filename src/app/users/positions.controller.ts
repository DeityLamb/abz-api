import { InjectRepository } from '@mikro-orm/nestjs';
import { SqlEntityRepository } from '@mikro-orm/postgresql';
import { Controller, Get, NotFoundException } from '@nestjs/common';
import { Position } from '../../domain/user/position.entity';

@Controller('/users')
export class UsersController {
  constructor(
    @InjectRepository(Position)
    private readonly positions: SqlEntityRepository<Position>,
  ) {}

  @Get()
  public async getPositions() {
    const positions = await this.positions.find({});

    if (!positions?.length) {
      throw new NotFoundException('Page not found');
    }

    return { positions };
  }
}
