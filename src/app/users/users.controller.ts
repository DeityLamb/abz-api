import {
  Body,
  Controller,
  Get,
  Headers,
  HttpStatus,
  Param,
  ParseFilePipeBuilder,
  ParseIntPipe,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { PaginationQuery } from '../../common';
import { UserService } from '../../domain/user';
import { CreateUserDto } from './dto/create-user.dto';
import { PositionProjection } from './dto/position.projection';
import { UserProjection } from './dto/user.projection';

@Controller('/')
export class UsersController {
  constructor(private readonly users: UserService) {}

  @Get('/positions')
  public async getPositions() {
    const positions = await this.users.getPositions();

    return { positions: positions.map(PositionProjection.fromEntity) };
  }

  @Get('/users')
  public async getUsers(@Query() query: PaginationQuery) {
    const [users, total_users] = await this.users.pages({
      orderBy: { createdAt: 'DESC' },
      offset: query.getOffset(),
      limit: query.count,
    });

    return {
      total_users,
      ...query.metadata('users', total_users),
      users: users.map(UserProjection.fromEntity),
    };
  }

  @Get('users/:user_id')
  public async getUser(
    @Param('user_id', ParseIntPipe)
    id: number,
  ) {
    const user = await this.users.findOne(id);

    return { user: UserProjection.fromEntity(user) };
  }

  @Post('users')
  @UseInterceptors(FileInterceptor('photo'))
  public async createUser(
    @Body()
    dto: CreateUserDto,
    @Headers('token')
    token: string,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: /jpe?g/i,
        })
        .addMaxSizeValidator({
          // 5 MB
          maxSize: 5_000_000,
        })
        .build({
          errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
          fileIsRequired: true,
        }),
    )
    photo: Express.Multer.File,
  ) {
    const user = await this.users.create({
      ...dto,
      token,
      photo,
    });

    return { user: UserProjection.fromEntity(user) };
  }
}
