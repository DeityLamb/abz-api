import { Module } from '@nestjs/common';
import { TokenModule } from './token/token.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [TokenModule, UsersModule],
})
export class AppModule {}
