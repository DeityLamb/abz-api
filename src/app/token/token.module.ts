import { Module } from '@nestjs/common';
import { DomainTokenModule } from '../../domain/token';
import { TokenController } from './token.controller';

@Module({
  imports: [DomainTokenModule],
  controllers: [TokenController],
})
export class TokenModule {}
