import { Controller, Get } from '@nestjs/common';
import { TokenService } from '../../domain/token';

@Controller('token')
export class TokenController {
  constructor(private readonly tokenService: TokenService) {}

  @Get()
  public async generate(): Promise<{ token: string }> {
    const token = await this.tokenService.generate();

    return { token: token.value };
  }
}
