import { Migration } from '@mikro-orm/migrations';

export class Migration20240402111045 extends Migration {
  async up(): Promise<void> {
    this.addSql(
      'create table "one_time_token" ("id" serial primary key, "value" varchar(255) not null, "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now());',
    );
    this.addSql(
      'create index "one_time_token_value_index" on "one_time_token" ("value");',
    );
  }
}
