import { Migration } from '@mikro-orm/migrations';

export class Migration20240403104026 extends Migration {
  async up(): Promise<void> {
    this.addSql(
      'create table "position" ("id" serial primary key, "name" varchar(255) not null, "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now());',
    );

    this.addSql(
      'create table "user" ("id" serial primary key, "name" varchar(255) not null, "email" varchar(255) not null, "phone" varchar(255) not null, "photo" varchar(255) not null, "position_id" int not null, "created_at" timestamptz not null default now(), "updated_at" timestamptz not null default now());',
    );

    this.addSql(
      'alter table "user" add constraint "user_position_id_foreign" foreign key ("position_id") references "position" ("id") on update cascade;',
    );
  }
}
