import { faker } from '@faker-js/faker';
import type { EntityManager } from '@mikro-orm/core';
import { Seeder } from '@mikro-orm/seeder';
import { Position, User } from '../../domain/user';

export class UsersSeeder extends Seeder {
  async run(em: EntityManager): Promise<void> {
    const positions = Array(4)
      .fill(null)
      .map(() => em.create(Position, { name: faker.person.jobTitle() }));

    const users = Array(45)
      .fill(null)
      .map(() =>
        em.create(User, {
          name: faker.person.fullName(),
          email: faker.internet.email(),
          phone: faker.helpers.fromRegExp('+380[0-9]{9}'),
          position: faker.helpers.arrayElement(positions),
          photo: new URL(
            'uploads/photo/default.jpg',
            process.env.SERVICE_URL,
          ).toString(),
          createdAt: faker.date.recent({ days: 30 }),
        }),
      );

    em.persist([...positions, ...users]);

    await em.flush();
  }
}
