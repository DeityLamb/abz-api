import { BadRequestException, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import 'dotenv/config';
import tinify from 'tinify';
import { HttpExceptionFilter, ResponseInterceptor } from './infra/http';
import { RootModule } from './root.module';

tinify.key = process.env.TINIFY_API_KEY;

async function bootstrap() {
  const app = await NestFactory.create(RootModule);

  app.enableCors({
    credentials: true,
    origin: ['http://localhost:4000', 'https://abz.deitylamb.me'],
  });

  app.useGlobalInterceptors(new ResponseInterceptor());
  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      transformOptions: { enableImplicitConversion: true },
      exceptionFactory: (errors) =>
        new BadRequestException('Validation failed', { cause: errors }),
    }),
  );
  app.useGlobalFilters(new HttpExceptionFilter());
  await app.listen(process.env.PORT || 3000);
}

bootstrap();
