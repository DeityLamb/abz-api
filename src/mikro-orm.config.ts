import { Migrator } from '@mikro-orm/migrations';
import { PostgreSqlDriver, defineConfig } from '@mikro-orm/postgresql';
import { SeedManager } from '@mikro-orm/seeder';
import 'dotenv/config';

export default defineConfig({
  entities: ['./dist/domain/**/*.entity.js'],
  entitiesTs: ['./src/domain/**/*.entity.ts'],
  driver: PostgreSqlDriver,
  clientUrl: process.env.POSTGRES_CONNECTION as string,
  debug: true,
  extensions: [Migrator, SeedManager],
  migrations: {
    dropTables: false,
    tableName: 'migrations',
    path: './dist/infra/migrations',
    pathTs: './src/infra/migrations',
    snapshot: false,
    allOrNothing: true,
    transactional: true,
  },
  seeder: {
    path: './dist/src/infra/seeders',
    pathTs: './src/infra/seeders',
  },
});
