import { Type } from 'class-transformer';
import { IsInt, IsOptional, Max, Min, ValidateIf } from 'class-validator';

export interface PaginationMetadata {
  page: number;
  total_pages: number;
  offset: number;
  count: number;
  links: {
    next_url: string | null;
    prev_url: string | null;
  };
}

export class PaginationQuery {
  @Type(() => Number)
  @IsInt()
  @Min(1)
  @ValidateIf((o) => !o.offset)
  public page = 1;

  @Type(() => Number)
  @IsInt()
  @Min(1)
  @ValidateIf((o) => !o.page)
  public offset: number;

  @Type(() => Number)
  @IsOptional()
  @IsInt()
  @Min(1)
  @Max(100)
  public count = 5;

  public getOffset(): number | null {
    const offset = this.offset || (this.page - 1) * this.count;

    return offset > 0 ? offset : null;
  }

  public metadata(endpoint: string, total: number): PaginationMetadata {
    const offset = this.getOffset();

    return {
      total_pages: Math.ceil(total / this.count),
      page: this.page,
      offset: this.offset,
      count: this.count,
      links: {
        next_url:
          offset + this.count >= total ? null : this.getNextPage(endpoint),
        prev_url: this.getPrevPage(endpoint),
      },
    };
  }

  private getNextPage(endpoint: string): string | null {
    const url = new URL(endpoint, process.env.SERVICE_URL);

    url.search = new URLSearchParams({
      ...(this.page
        ? { page: String(this.page + 1) }
        : { offset: String(this.offset + this.count) }),
      count: String(this.count),
    }).toString();

    return url.toString();
  }

  private getPrevPage(endpoint: string): string | null {
    if (this.offset <= this.count || this.page === 1) {
      return null;
    }

    const url = new URL(endpoint, process.env.SERVICE_URL);

    url.search = new URLSearchParams({
      ...(this.page
        ? { page: String(this.page - 1) }
        : { offset: String(this.offset - this.count) }),
      count: String(this.count),
    }).toString();

    return url.toString();
  }
}
