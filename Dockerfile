FROM node:18 as build
ARG PORT
ARG POSTGRES_CONNECTION
EXPOSE ${PORT}

WORKDIR /opt/app

VOLUME /opt/app/uploads
COPY uploads /opt/app/uploads

ADD . .
RUN yarn
RUN yarn build

CMD ["yarn", "start:prod"]
